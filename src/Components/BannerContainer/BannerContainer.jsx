import React, { PureComponent } from "react";
import "./BannerContainer.css";
import BannerImage from "./BannerImage.jsx";
import InstaData from "/home/madhurachanna/Documents/JSAssignment/instagram-clone-react/src/dummy-data.js";
import BannerHeader from "./BannerHeader.jsx";
import BannerContent from "./BannerContent.jsx";

class BannerContainer extends PureComponent {
  state = {};
  render() {
    // const bannerHeaders = InstaData.map((_, index) => (

    // ));
    const cards = InstaData.map((_, index) => {
      let x = 0;
      return (
        <div className="wrap">
          <BannerHeader
            imgUrl={InstaData[index]["thumbnailUrl"]}
            userName={InstaData[index]["username"]}
          />
          <BannerImage imgUrl={InstaData[index]["imageUrl"]} />
          <BannerContent
            numbOfLikes={InstaData[index]["likes"]}
            userName1={InstaData[index]["comments"][x]["username"]}
            data1={InstaData[index]["comments"][x]["text"]}
            userName2={InstaData[index]["comments"][x + 1]["username"]}
            data2={InstaData[index]["comments"][x + 1]["text"]}
            userName3={InstaData[index]["comments"][x + 2]["username"]}
            data3={InstaData[index]["comments"][x + 2]["text"]}
          />
        </div>
      );
    });

    return <div className="bannerContainer">{cards}</div>;
  }
}

export default BannerContainer;
