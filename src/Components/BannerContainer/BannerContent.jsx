import React, { PureComponent } from "react";
import "./BannerContainer.css";

class BannerContent extends PureComponent {
  state = {};
  render() {
    return (
      <div className="bannerContent">
        <div className="numbOfLikes">{this.props.numbOfLikes} likes </div>
        <div>
          <span id="commentName">{this.props.userName1}</span>
          <span id="comment">{this.props.data1}</span>
        </div>
        <div>
          <span id="commentName">{this.props.userName2}</span>
          <span id="comment">{this.props.data2}</span>
        </div>
        <div>
          <span id="commentName">{this.props.userName3}</span>
          <span id="comment">{this.props.data3}</span>
        </div>
      </div>
    );
  }
}

export default BannerContent;
