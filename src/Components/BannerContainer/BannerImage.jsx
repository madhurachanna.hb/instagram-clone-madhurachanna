import React, { PureComponent } from "react";
import "./BannerContainer.css";

class BannerImage extends PureComponent {
  state = {};
  render() {
    return (
      <div className="bannerImage">
        <img src={this.props.imgUrl} />
      </div>
    );
  }
}

export default BannerImage;

// https://scontent-sin6-2.cdninstagram.com/vp/ad7347ceb7e38fc51166c946821bb3b0/5D462E67/t51.2885-15/e35/25005865_130523064311747_2658488669215653888_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com
