import React, { PureComponent } from "react";
import "./BannerContainer.css";

class BannerHeader extends PureComponent {
  state = {};
  render() {
    return (
      <div className="bannerHeader">
        <img src={this.props.imgUrl} alt="" />
        <h3>{this.props.userName}</h3>
      </div>
    );
  }
}

export default BannerHeader;
