import React, { PureComponent } from "react";
import "./SearchBar.css";

class SearchBarLogo extends PureComponent {
  state = {};
  render() {
    return (
      <div className="searchBarLogo">
        <img
          src={require("/home/madhurachanna/Documents/JSAssignment/instagram-clone-react/src/Images/InstaLogo.png")}
          alt="Logo"
        />
      </div>
    );
  }
}

export default SearchBarLogo;
