import React, { PureComponent } from "react";
import "./SearchBar.css";
import SearchBarLogo from "./SearchBarLogo.jsx";
import SearchButton from "./SearchButton.jsx";

class SearchBar extends PureComponent {
  state = {};
  render() {
    return (
      <div className="searchBar">
        <SearchBarLogo />
        <SearchButton />
      </div>
    );
  }
}

export default SearchBar;
