import React, { PureComponent } from "react";
import "./SearchBar.css";

class SearchButton extends PureComponent {
  state = {};
  render() {
    return (
      <div className="searchButtonBox">
        <input id="searchButton" type="text" placeholder="Search" />
      </div>
    );
  }
}

export default SearchButton;
