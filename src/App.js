import React, { Component } from "react";
import "./App.css";
import SearchBar from "./Components/SearchBar/SearchBar.jsx";
import BannerContainer from "./Components/BannerContainer/BannerContainer.jsx";

class App extends Component {
  render() {
    return (
      <div className="App">
        <SearchBar />
        <BannerContainer />
      </div>
    );
  }
}

export default App;
