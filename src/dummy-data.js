const dummyData = [
  {
    id: "a",
    username: "Despicable Me",
    thumbnailUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/f5b5043c529dc0aded9cd62254ccbd18/5D33E47B/t51.2885-19/s150x150/22278350_363587497405115_3327552176301015040_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    imageUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/ad7347ceb7e38fc51166c946821bb3b0/5D462E67/t51.2885-15/e35/25005865_130523064311747_2658488669215653888_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    likes: 400,
    timestamp: "July 17th 2017, 12:42:40 pm",
    comments: [
      {
        id: "1",
        username: "philzcoffee",
        text:
          "We've got more than just delicious coffees to offer at our shops!"
      },
      {
        id: "2",
        username: "biancasaurus",
        text: "Looks delicious!"
      },
      {
        id: "3",
        username: "martinseludo",
        text: "Can't wait to try it!"
      }
    ]
  },
  {
    id: "b",
    username: "MONSTERS,INC",
    thumbnailUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/b54bb8178e704b2a2956739a655a7f0e/5D406006/t51.2885-19/11349290_1154297877920693_1329741513_a.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    imageUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/5b1a63ea6842b3d992e91a3b3214fd5a/5D333F38/t51.2885-15/e15/11262869_1436987566606582_1526321323_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    likes: 4307,
    timestamp: "July 15th 2017, 03:12:09 pm",
    comments: [
      {
        id: "4",
        username: "twitch",
        text: "Epic Street Fighter action here in Las Vegas at #EVO2017!"
      },
      {
        id: "5",
        username: "michaelmarzetta",
        text: "Omg that match was crazy"
      },
      {
        id: "6",
        username: "themexican_leprechaun",
        text: "What a setup"
      },
      {
        id: "7",
        username: "dennis_futbol",
        text: "It that injustice"
      },
      {
        id: "8",
        username: "dennis_futbol",
        text: "Is"
      }
    ]
  }
];

export default dummyData;
